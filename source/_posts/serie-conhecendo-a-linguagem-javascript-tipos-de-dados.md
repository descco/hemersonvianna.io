---
title: Serie conhecendo a linguagem Javascript - Tipos de dados
date: 2015-05-11 15:15:10
update: 2015-10-06 15:15:10
category: javascript
tags:
---

A linguagem JavaScript é uma linguagem de tipagem fraca (ou tipagem dinâmica). Isso significa que variáveis JavaScript não têm nenhum tipo predeterminado. Assim, o tipo de uma variável é o tipo do seu valor.

Em JavaScript, você pode realizar operações em valores de diferentes tipos sem causar uma exceção. O interpretador converte implicitamente um dos tipos de dados no outro e realiza a operação. 

Regras para os tipos primários:

```
// string + number
var test = '1' + 1;
console.log(typeof test); // string

// string + boolean
var test2 = '1' + false;
console.log(typeof test2); // string

// number + boolean
var test3 = 1 + false;
console.log(typeof test3); // number

```

### Primários

Os tipos de dados primários (primitivos) são:

String
```
var name = 'Hemerson';
console.log(typeof name); // string
```

Number
```
var age = 29;
console.log(typeof age); // number
```

Boolean
```
var rich = false;
console.log(typeof rich); // boolean
```

Symbol (Novidade com a versão ECMAScript 2015)
```
var symbol1 = Symbol('foo');
console.log(typeof symbol1); // symbol
```


### Compostos

Os tipos de dados compostos (de referência) são:

Object
```
var obj = {};
console.log(typeof obj); // object
```

Array
```
var items = [];
console.log(items instanceof Array); // true
```


### Especiais

Os tipos de dados especiais são:

Null
```
var money = null;
console.log(money); // null
```

Undefined
```
console.log(typeof lost); // undefined
```

<br><br>
### Série Conhecendo a linguagem JavaScript

*	1.[Introdução](/javascript/serie-conhecendo-a-linguagem-javascript-introducao)
*	2.Tipos de dados
*	3.[Valores](/javascript/serie-conhecendo-a-linguagem-javascript-valores)
*	4.[Variáveis](/javascript/serie-conhecendo-a-linguagem-javascript-variaveis)
