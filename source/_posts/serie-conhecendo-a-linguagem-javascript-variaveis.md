---
title: Serie conhecendo a linguagem Javascript - Variaveis
date: 2015-05-21 15:15:27
update: 2015-10-08 15:15:27
category: javascript
tags:
---

Variáveis são endereços de memória onde armazenamos dados ou informações. Precisamos declarar as mesmas, para manipular os dados e informações com facilidade e para termos controle durante os processos.

Até a quinta versão do ECMA-262, variáveis não declaradas ou sem valor receberiam o valor `undefined`. Agora com a sexta versão, variáveis declaradas sem valor e não declaradas, terão o valor `string` e `undefined`, respectivamente.

```
// versão 5
var name;
console.log(typeof name); // undefined
console.log(typeof calc); // undefined
```

```
// versão 6
var name;
console.log(typeof name); // string
console.log(typeof calc); // undefined
```

Podemos apenas declarar uma variável sem valor, com isso já estaremos configurando na memória para o seu uso posteriormente. Declarando já com um valor, dizemos que a variável está sendo inicializada.

```
var name = 'Hemerson';
```


É possível declarar uma variável sem usar a palavra-chave var na declaração e atribuir um valor para ela. Trata-se de uma declaração implícita.

```
name = 'Hemerson';
```

Não é possível usar uma variável que nunca tenha sido declarada.

```
var result = width * height;
```


## Nomeação

A linguagem JavaScript é '**case sensitive**', portanto faz distinção entre maiúsculas e minúsculas.

```
var name = 'hemerson';
var Name = 'fulano';

console.log(name) // hemerson
console.log(Name) // fulano
```

Regras para nomear uma variável:

*	1. O caractere inicial deve ser uma letra ASCII ou um caractere de sublinhado(_). E não pode iniciar por um número
*	2. Os próximo caracteres devem ser letras, números ou sublinhados (_).
*	3. Não pode ter o nome de uma palavra reservada.


### Palavras reservadas

break - default - function - return - var - case - delete - if - switch - void - catch - do - in - this - while - const - else - instanceof - throw - with - continue - finally - let - try - debugger - for - new - typeof


Pode:

```
var _name;
var Age;
var match_result;
```

Não pode:

```
var 107Test;
var name&age;
```


## E aí, comma-first ou trailing commas?

Comma-first:

```
var name = 'hemerson'
    , age= 28
;
```

Trailing commas:

```
var name = 'hemerson',
    age= 28;
```

O padrão tradicional e o qual muitos estão acostumados, seria com as vírgulas à direita.  Com a vírgula na frente, na minha opinião tem facilitado bastante a legibilidade do código e dependendo da ocasião, facilidade para comentar uma variável.

Sendo adepto do comma-first, deixo um [artigo do Suissa](http://nomadev.com.br/comma-first-por-que-usar/) que levanta uns bons pontos.

Essa é uma questão que mesmo tendo uma preferência, deixo aberto que a decisão seja tomada visando o melhor para o projeto e padronização da equipe.


<br><br>
### Série Conhecendo a linguagem JavaScript

*	1.[Introdução](/javascript/serie-conhecendo-a-linguagem-javascript-introducao)
*	2.[Tipos de dados](/javascript/serie-conhecendo-a-linguagem-javascript-tipos-de-dados)
*	3.[Valores](/javascript/serie-conhecendo-a-linguagem-javascript-valores)
*	4.Variáveis