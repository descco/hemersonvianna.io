---
title: Tarefas automatizadas com gruntjs
date: 2015-10-08 15:17:22
update: 2015-10-15 15:17:22
category: front-end
---

No desenvolvimento Front-End tem algumas tarefas que são como um castigo se feitas manualmente. Por isso, temos que automatizar algumas tarefas. Para nos auxiliar com isso, temos o GruntJS.

O GruntJS é um `task runner` que automatiza tarefas e roda via terminal. 

## Instalação

Para rodá-lo, precisamos primeiramente ter o NodeJS e NPM, instalados.

Podendo ser baixado, aqui: [NodeJS com NPM](https://nodejs.org/en/download/)

Agora que já temos o NodeJS e NPM, instalados. Temos que instalar o GruntJS via terminal:

```
npm install -g grunt-cli
```

Podemos ver se está instalado com o commando:
```
grunt --version
```

Para o GruntJS funcionar, precisamos de 2 arquivos: `package.json` e o `Gruntfile.js`.

O ***package.json*** é um arquivo do NodeJS, onde terão as informações do projeto e os plugins do GruntJS. Ex:

```
{
  "name": "project name",
  "version": "1.0.0",
  "title": "Project Title",
  "devDependencies": {
    "grunt": ">=0.4.5",
    "load-grunt-config": "^0.10.0",
    "load-grunt-tasks": "^0.4.0"
  },
}
```

O ***Gruntfile.js*** é o arquivo de configuração do GruntJS. Ex:

```
module.exports = function( grunt ) {
 
  grunt.initConfig({
    // Tasks
  });
};
```

Para adicionar um plugin no seu `package.json`, podemos utilizar os comandos:

Para as dependências para produção (`dependencies`) 
```
npm install grunt --save
```

Para dependências, apenas para o desenvolvimento (`devDependencies`)
```
npm install grunt --save-dev
```

Se você está utilizando um `package.json`, já com os plugins listados, basta executar o comando:
```
npm install
```

## Configuração

Agora com os plugins instalados, precisando configurar cada tarefa para ser executado ao rodar o `GruntJS`.

No link [http://gruntjs.com/plugins](http://gruntjs.com/plugins), você pode conferir os plugins para o `GruntJS`. Clicando no plugin desejado, será encaminhado para o host [https://www.npmjs.com](https://www.npmjs.com), onde terá as informações para a configuração do `package` escolhido. 

Como exemplo, escolhi o plugin `grunt-contrib-uglify`, sendo levado para [https://www.npmjs.com/package/grunt-contrib-uglify](https://www.npmjs.com/package/grunt-contrib-uglify). Onde podemos conferir, como configurar a tarefa.

Abaixo, como podemos ver no método `initConfig`, iremos passar um objeto com a configuração do plugin. Com o método `loadNpmTasks`, informaremos a tarefa que deverá ser carregada. E para finalizar, devemos registrar as tarefas que teremos no projeto.
```
module.exports = function( grunt ) {

  grunt.initConfig({

    uglify: {
	    my_target: {
			files: {
				'dest/output.min.js': ['src/input1.js', 'src/input2.js']
			}
	    }
	}

  });


  // Plugins do Grunt
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );


  // Tarefas que serão executadas
  grunt.registerTask( 'default', [ 'uglify' ] );

};

```

Para ficar mais claro, temos outro exemplo com mais plugins e tarefas.

```
module.exports = function( grunt ) {

  grunt.initConfig({

    uglify: {
	    my_target: {
			files: {
				'dest/output.min.js': ['src/input1.js', 'src/input2.js']
			}
	    }
	},
	cssmin: {
		target: {
	    	files: [{
				expand: true,
				cwd: 'release/css',
				src: ['*.css', '!*.min.css'],
				dest: 'release/css',
				ext: '.min.css'
			}]
		}
	}

  });


  // Plugins do Grunt
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  grunt.loadNpmTasks( 'grunt-contrib-cssmin' );


  // Tarefas que serão executadas
  grunt.registerTask( 'default', [ 'cssmin', 'uglify' ] );
  grunt.registerTask( 'css', [ 'cssmin' ] );
  grunt.registerTask( 'js', [ 'uglify' ] );

};

```

Acima, temos a configuração dos plugins `cssmin` e `uglify` no `Gruntfile.js`, onde os mesmos serão carregados com o método `loadNpmTasks` e registramos 3 tarefas.


## Executando as tarefas 

No terminal, rodando o comando:
```
grunt
```
Será executada a lista de tarefas `default` (`cssmin` e `uglify`)

Com o comando:
```
grunt css
```

Será executada a lista `css` 

E o comando:

```
grunt js
```
a lista `js`

Acredito que ficou simples de compreender a lógica para utilizar as tarefas do `GruntJS`. Com isso, temos tudo instalado e configurado. Agora só ser feliz com as tarefas automatizadas.

## A dica

Dependendo da quantidade de configurações no seu `Gruntfile.js`, a manutenção fica bem complicada. Para resolver o problema, podemos utilizar o plugin `load-grunt-config`, para termos mais organização.

[https://github.com/firstandthird/load-grunt-config](https://github.com/firstandthird/load-grunt-config)
O ***load-grunt-config***, permite quebrar a configuração do `Gruntfile.js` por tarefa. Você pode usar os arquivos na extensão .js , .json , .yaml , .cson , ou .coffee.


Instalando o plugin
```
npm install load-grunt-config --save-dev
```

O `Gruntfile.js`, ficará semelhante ao exemplo abaixo:
```
module.exports = function(grunt) {
    // Carrega configurações da pasta grunt-configs/
    var path = require('path');

    require('load-grunt-config')(grunt, {
        init: true,
        configPath: path.join(process.cwd(), 'source/_tasks'),
        data: {
            projectDir  : '../../public/assets/',
            projectDev  : 'source/_assets/',
            pkg: grunt.file.readJSON('package.json')
        }
    });

};
```

`'source/_tasks'` - Será o caminho para onde estão os arquivos com as configurações das tarefas.

No objeto data, informamos as variáveis que será retornadas pelo parâmetro `options` no arquivo de configuração de cada plugin, como podemos ver abaixo. 
```
module.exports = function(grunt, options){
	// configuração
};
```

No `Gruntfile.js`, o caminho das configurações se encontram na pasta `source/_tasks` e a estrutura ficaria assim:
```
>source
  >_tasks
    > aliases.yaml
    > cssmin.js
```

A sua pasta com as tarefas precisará ter o arquivo `aliases.(js|.json|yaml|cson|coffee)` para definir as suas tarefas.

Exemplo com a extensão `.yaml`:

***aliases.yaml***
```
default:
  - 'cssmin'
  - 'uglify'

css:
  - 'cssmin'

js:
  - 'uglify'

```

Abaixo, um exemplo com a extensão `.js`, da configuração de um plugin. 

***cssmin.js***
```
module.exports = function(grunt, options){

  var projectDir = options.projectDir;

  return {
    site: {
      files: [{
        expand: true,
        cwd: '<%= projectDir %>css/site/',
        src: ['*.css'],
        dest: '<%= projectDir %>css/site/'
      }]
    }
  };
};
```

E assim, podemos utilizar o `GruntJS`, para estar automatizando as tarefas que dão um bom trabalho se feitas manualmente e poupam um bom tempo, nos deixando mais produtivos.