---
title: Serie conhecendo a linguagem Javascript - Introdução
date: 2015-05-07 15:14:55
update: 2015-10-05 15:14:55
category: javascript
tags:
---

E aqui se inicia a série "Conhecendo a linguagem JavaScript". A ideia veio depois de ver um número grande de front-enders que ainda não se preocupam em conhecer a linguagem e só apenas usam frameworks e bibliotecas.

Quando perguntado se conhece a linguagem, a resposta mais ouvida foi: ***'Uso jQuery!'***

Então, vamos falar sobre a linguagem JavaScript, que já foi chamada de Mocha, posteriormente LiveScript e por fim JavaScript. A mesma foi criada por [@BrendanEich](https://twitter.com/brendaneich), e teve sua primeira versão JavaScript 1.0, introduzida no navegador Netscape 2.0 em 1996.

>JavaScript é uma linguagem de programação interpretada . Foi originalmente implementada como parte dos navegadores web para que scripts pudessem ser executados do lado do cliente e interagissem com o usuário sem a necessidade deste script passar pelo servidor, controlando o navegador, realizando comunicação assíncrona e alterando o conteúdo do documento exibido.
> <cite> -- JavaScript
> - [Wikipédia](http://pt.wikipedia.org/wiki/JavaScript)
> </cite>

> Com JavaScript, podemos controlar o comportamento do navegador em diversos aspectos, como criar janelas pop-up, apresentar mensagens ao usuário, alterar as dimensões do navegador, interferir na barra de status, retirar menus, fechar e abrir janelas.
>
> JavaScript é capaz de acessar os campos e valores digitados em um formulário HTML e proceder à validação dos dados, realizar cálculos e fornecer dicas de preenchimento dos campos.
>
> <cite> -- JavaScript Guia do Programador
> - Maurício Samy Silva
> </cite>


Atualmente, o nome oficial da linguagem JavaScript é ECMAScript e a versão é a ECMA-262 v6 (ECMAScript 2015).

### Tabela 1.1 – Versões da JavaScript

| Versão    			|   Implementação    	|   Mês/ano
|-----------------------|-----------------------|---------------
| JavaScript 1.0    	| Netscape 2.0    		|   Março 1996
| JavaScript 1.1    	| Netscape 3.0    		|   Agosto 1996
| JavaScript 1.2    	| Netscape 4.0 e 4.05 	|   Junho 1997
| JavaScript 1.3    	| Netscape 4.06 e 4.07x |   Outubro 1998
| JavaScript 1.4    	| Servidores Netscape	|   -
| JavaScript 1.5    	| Netscape 6.0 – Firefox 1.0 – Opera 6.0 a 9.0    		|   Novembro 2000
| JavaScript 1.6    	| Firefox 1.5 – Safari 3.0 e 3.1    		|   Novembro 2005
| JavaScript 1.7    	| Firefox 2.0 – Safari 3.2 e 4.0 – Chrome 1.0    		|   Outubro 2006
| JavaScript 1.8    	| Firefox 3.0    		|   Junho 2008
| JavaScript 1.8.1   	| Firefox 3.5    		|   2008
| JavaScript 1.9   		| Firefox 4.0    		|   2009
| JavaScript 1.0   		| Internet Explorer 3    		|   Agosto 1996
| JavaScript 2.0   		| Internet Explorer 3 – Windows IIS 3    		|   Janeiro 1997
| JavaScript 3.0   		| Internet Explorer 4	|   Outubro 1997
| JavaScript 4.0   		| Visual Studio 6.0		|   -
| JavaScript 5.0   		| Internet Explorer 5	|   Março 1999
| JavaScript 5.1   		| Internet Explorer 5.01|   -
| JavaScript 5.5   		| Internet Explorer 5.5 |   Julho 2000
| JavaScript 5.6   		| Internet Explorer 	|   Outubro 2001
| JavaScript 5.7   		| Internet Explorer 7	|   Novembro 2006
| JavaScript 5.8   		| Internet Explorer 8	|   Março 2009
| ECMA-262 v1      		| Navegadores versão 4	|   1998
| ECMA-262 v2   		| Versão de testes		|   1998
| ECMA-262 v3   		| Navegadores versão 6	|   1999
| ECMA-262 v4   		| Navegadores versão 6+	|   2002
| ECMA-262 v5   		| Navegadores versão 6+	|   2009
| ECMA-262 v6       | Versão atual      |   2015


> ECMA (acrônimo para European Computer Manufacturers Association) é uma associação fundada em 1961 dedicada à padronização de sistemas de informação. Desde 1994 passou a se denominar Ecma International para refletir suas atividades internacionais. A associação é aberta a companhias que produzem, comercializam ou desenvolvem sistemas de computação ou de comunicação na Europa.
> <cite> -- Ecma International
> - [Wikipédia](http://pt.wikipedia.org/wiki/Ecma_International)
> </cite>


> ECMAScript é uma linguagem de programação baseada em scripts, padronizada pela Ecma International na especificação ECMA-262. A linguagem é bastante usada em tecnologias para Internet, sendo esta base para a criação do JavaScript/JScript e também do ActionScript.
> <cite> -- ECMAScript
> - [Wikipédia](http://pt.wikipedia.org/wiki/ECMAScript)
> </cite>


## Mãos à obra

Conseguimos inserir de algumas formas o código JavaScript em um documento HTML.

Incorporado no `<head>` ou no `<body>`:

```
<!doctype html>
<html>
  <head>
    <script type="text/javascript">
      // Insira o código Javascript aqui
    </script>
  </head>
  <body>
    <script type="text/javascript">
      // Insira o código Javascript aqui
    </script>
  </body>
</html>
```


Linkado no `<head>` ou no `<body>`:

```
<!doctype html>
<html>
  <head>
    <script type="text/javascript" src="arquivo.js"></script>
  </head>
  <body>  
  	<script type="text/javascript" src="arquivo.js"></script>
  </body>
</html>
```

Também é possível de forma `inline`, mas não é aconselhável por dificultar a manutenção e etc.

```
<!doctype html>
<html>
  <head>
  </head>
  <body>  
    <a href="#" onclick="document.getElementsByTagName('p')[0].style.color = 'red';return false;">clique aqui</a>

    <p>Text 1</p>
    <p>Text 2</p>

  </body>
</html>
```


## EcmaScript 2015

Já podemos utilizar as novas features sem preocupação com o auxílio de um "transpiler", como o [BabelJS](https://babeljs.io/).
Em um próximo artigo, falarei sobre as novas features.

Com isso, temos uma introdução na linguagem. :)

<br><br>
### Série Conhecendo a linguagem JavaScript

*	1.Introdução
*	2.[Tipos de dados](/javascript/serie-conhecendo-a-linguagem-javascript-tipos-de-dados)
*	3.[Valores](/javascript/serie-conhecendo-a-linguagem-javascript-valores)
*	4.[Variáveis](/javascript/serie-conhecendo-a-linguagem-javascript-variaveis)