title: O que faz um front-ender
date: 2015-02-08 14:19:49
category: front-end
tags:
---

Hoje venho falar sobre o que o desenvolvedor front-end faz. Existem dois tipos de linguagens para o desenvolvimento web:
As linguagens client-side(lado cliente) e server-side(lado servidor). Com isso, no desenvolvimento temos três estágios (Front-End, Middleware e Back-End).

**O Front-End está no lado cliente(client-side), no caso seria o primeiro estágio quando acessamos um site. Normalmente a estrutura front-end é composta por HTML, CSS e Javascript.**

O Middleware(mediador) seria o intermediário entre a interface(front) e os dados(back). Geralmente é associado a um "encanamento" de um sistema. Um exemplo seria a comunicação de aplicações via web service.

O Back-End está no lado servidor(server-side), que é responsável por todo processamento dos dados recebidos do front-end e esse estágio mexe com o banco de dados(ex: MySQL, MS-SQL, PostgreSQL e etc...) e o armazenamento de dados. Posso citar algumas linguagens do lado servidor como Ruby on Rails, Lua, Perl, Python, Django, Node, PHP e etc...

O intuito é apenas abordar superficialmente os dois últimos estágios citados e focar no primeiro estágio que seria o Front-End.

> Front-end e back-end são termos utilizados para caracterizar interfaces de programas e serviços em relação ao usuário inicial desses recursos. (O "usuário" pode ser um ser humano ou um programa.) A aplicação "front-end" é aquele que os usuários do aplicativo interagem diretamente. Um aplicativo de "back-end" ou programa serve indiretamente em apoio aos serviços de front-end, geralmente por estar mais perto do recurso necessário ou que tenham a capacidade de se comunicar com o recurso necessário.
> <cite> -- Front-end
> - [Margaret Rouse](http://whatis.techtarget.com/definition/front-end)
> </cite>

Abaixo você tem uma estrutura (HTML), um estilo para o link (CSS) e uma interação (Javascript). Essa é apenas uma pequena amostra do que um front-ender faz.

Exemplo HTML:

```
<p>
	<a id="link" href="/">Link</a>
</p>
```

Exemplo CSS:

```
#link {
	color: #4299E3;
	font-size: 14px;
}
```

Exemplo JavaScript:

```
var el = document.getElementById("link");

function message( event ) {
	event.preventDefault();
	alert('Este é um link');
}
el.addEventListener("click", message, false);
```

O código acima, funcionando: [Demonstração](http://jsfiddle.net/hc77v7g2/)

> ### O que o front-end faz:

> 1.   Estabelecer uma linguagem visual entre designers e o back-end
> 2.   A partir de um projeto visual, definir um conjunto de componentes que representam o conteúdo, marca, recursos, etc.
> 3.   Estabelecer uma linha de base para a aplicação Web em termos de convenções, quadros, requisitos, linguagens visuais e especificações.
> 4.   Definir o escopo do aplicativo Web em termos de dispositivos, navegadores, telas, animações.
> 5.   Desenvolver uma diretriz de garantia de qualidade para garantir a fidelidade da marca, a qualidade do código, revisão de produto pelas partes interessadas.
> 6.   Estilo de aplicações Web com espaçamentos adequados, tipografia, títulos, fontes cara, ícones, margens, preenchimentos, grades e assim por diante.
> 7.   Estilo de aplicações Web com várias imagens de alta resolução, mockups orientada dispositivo, tudo ao mesmo tempo cuidando de diretrizes de design.
> 8.   Marcação da aplicação web, tendo em conta a semântica, acessibilidade, SEO, esquemas e microformats.
> 9.   Conectar-se a uma API para recuperar informações em um amistoso, consumindo não-bateria, dispositivo e cliente maneira consciente.
> 10.   Desenvolver código do lado do cliente para executar animações, transições, carregamento lento, interações, fluxos de trabalho de aplicativos, a maior parte do tempo, tendo em conta a optimização progressiva e normas para trás compatíveis.
> 11.   Certificar que as conexões de back-end são seguras.
> 12.   Nunca esquecendo que apesar de prazos rigorosos, os pedidos dos associados e as limitações do dispositivo, o usuário é, e estará sempre em primeiro lugar.
>
> <cite> -- Why can’t we find Front End developers?
> - [Jose Aguinaga](http://jjperezaguinaga.com/2014/03/19/why-cant-we-find-front-end-developers/)
> </cite>


Em um projeto web, temos a regra de negócio:

> "Regras de Negócio são declarações sobre a forma da empresa fazer negócio. Elas refletem políticas do negócio. As organizações com isto têm políticas para satisfazer os objetivos do negócio, satisfazer clientes, fazer bom uso dos recursos, e obedecer às leis ou convenções gerais do negócio.
> Regras do Negócio tornam-se requisitos, ou seja, podem ser implementados em um sistema de software como uma forma de requisitos de software desse sistema. Representam um importante conceito dentro do processo de definição de requisitos para sistemas de informação e devem ser vistas como uma declaração genérica sobre a organização.
> As regras de negócio definem como o seu negócio funciona, podem abranger diversos assuntos como suas políticas, interesses, objetivos, compromissos éticos e sociais, obrigações contratuais, decisões estratégicas, leis e regulamentações entre outros."
> <cite> -- Regras de negócio
> - Wikipédia
> </cite>

Com essa definição, passamos para próximo passo que seria o UX/UI design (Desenho da experiência do usuário ou desenho da Interface do usuário)

> "De um modo simplificado, podemos dizer que em um projeto de um carro, por exemplo, a interface (também chamado de User Interface, Interface de Usuário ou apenas UI) é toda a parte “física” do veiculo; ou seja, a forma. Já a experiência (ou User Experience, UX) é o prazer que o veiculo te proporciona ao dirigi-lo; ou seja, a sua experiência de uso do veículo."
> <cite> -- Design Básico: Diferença entre UX e UI Design
> - [Canha - Chocolate Design](http://chocoladesign.com/design-basico-diferenca-entre-ux-e-ui-design)
> </cite>

Essa etapa com a regra de negócio em mãos, começa um estudo para ter a melhor forma de apresentação e experiência para o usuário.

Após esse estudo, chegamos na etapa do design final. Onde todos os elementos que se mostraram necessários para a apresentação do negócio/produto ganham cores e formas mais harmônicas para o usuário. E assim, temos o layout final do projeto. Geralmente, o front-ender recebe os layouts das telas em formato PSD(Photoshop).

Agora o trabalho do front-ender é transformar essas telas em páginas web, onde serão acessadas pelo navegador, como o exemplo de código que citei no inicio desse artigo.

Em teoria, o back-end seria a última etapa, mas na maioria das vezes em paralelo ou antes, pois o front-end precisa saber como enviar alguns dados por meio de API e etc...

> "No contexto de desenvolvimento Web, uma API é um conjunto definido de mensagens de requisição e resposta HTTP, geralmente expressado nos formatos XML ou JSON. A chamada Web 2.0 vem abandonando o modelo de serviços SOAP em favor da técnica REST.
> Enquanto você usufrui de um aplicativo ou sítio, este pode estar conectada a diversos outros sistemas e aplicativos via APIs sem que se perceba.
> Um exemplo popular é a rede social Twitter, sendo possível ler e publicar mensagens."
> <cite> -- Interface de programação de aplicações
> - Wikipédia
> </cite>

Bom, a ideia foi mostrar um pouco do processo e aonde se encaixa o front-ender. O front-end é o elo entre o layout e o armazenamento de dados. Além de ser a estrutura da interface para o usuário, ele tem algumas preocupações a mais, como comunicação com o back-end, ser adaptável para todos os dispositivos e diferentes navegadores, código limpo e bem estruturado para o buscadores e etc..

Espero que tenha contribuído, com esse meu primeiro artigo.

Aceitando feedbacks. :)