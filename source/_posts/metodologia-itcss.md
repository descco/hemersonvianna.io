title: Metodologia ITCSS
date: 2015-04-13 15:12:13
category: css
tags:
---

Olá, pessoal.

Vou escrever sobre uma coisa muito importante, que é o gerenciamento do código CSS. Você começa um projeto Front-End e está na hora de pensar como será a arquitetura CSS.

Se tem várias metodologias e frameworks no mercado. Com isso, várias fontes de estudo e conceitos mais sólidos para uma implantação.

Hora da avaliação dos pós e contras, e escolher o melhor rumo para o seu  projeto. Nesse artigo, vou escrever sobre a metodologia que chamou a minha atenção e tenho adotado.

## No que tenho que pensar?

Quando vamos começar um projeto web, temos que montar uma boa arquitetura CSS, para que não tenhamos problemas futuramente. E é bom seguir a ideia de "perder" algumas horas de planejamento no início, do que perder dias para consertar e/ou mudar a metodologia adotada no meio do projeto.

O ponto essecial para uma arquitetura CSS é o mesmo ser modular (Escabilidade, reutilização e customização), principalmente para projetos complexos. O mesmo deve ser bem gerenciado, para não ter dor de cabeça com hierarquia e etc..

Levanta a mão que já pegou um projeto legado com vários "!important", sem nenhum critério. .../

**Portanto, temos que nos preocupar com muitos fatores na hora de escrever um código CSS. Como:**

- - Escolha do pré-processador
- - Padrão de escrita
- - Estrutura de pastas
- - Ser modular (escalável, reutilizável e customizável)
- - Metodologias
- - O melhor para a equipe/projeto

Com a metodologia ITCSS, uma boa porcentagem das preocupações serão resolvidas.


## ITCSS - Inverted triangle CSS

Na busca pela melhor forma de gerenciar o CSS dos meus projetos, conheci a metodologia [ITCSS](https://twitter.com/itcss_io), que foi criada por [Harry Roberts](https://twitter.com/csswizardry).

Aqui você confere o vídeo em que foi apresentado a metodologia [Harry Roberts - Managing CSS Projects with ITCSS](https://www.youtube.com/watch?v=1OKZOV-iLj4&hd=1)

Slides - [Managing CSS Projects with ITCSS](https://speakerdeck.com/dafed/managing-css-projects-with-itcss)

### A metodologia

A metodologia consiste em camadas e aborda a escrita por ordem de especificidade, onde as 2 primeiras (Settings e tools), são utilizadas no desenvolvimento composto por um pré-processador de CSS e as camadas generic, base, objects, components, theme(sendo optativa) e trumps.
Com isso:

- evitando a redundância de regras de estilo;
- problemas com especificidade, indo da mais baixa para a mais alta;
- código reutilizavel, com uma estrutura granular;
- elimina a necessidade de mais desenvolvimento de código para solucionar questões de substituição de propriedades.


![ITCSS](/assets/app/css/img/site/articles/arquitetura-itcss.jpg)

**[Settings] Configurações:**

Utilizando um pré-processador de CSS, essa camada inicial tem todo o controle de variáveis ​​globais e configuração do projeto (cores, tamanhos de tela).

```
// Color palette
$color-brand:           #bada55;
$color-brand-highlight: lighten($color-brand, 15%);
$color-brand-shadow:     darken($color-brand, 15%);

// Links
$color-link:        $color-brand;
$color-link-hover:  $color-brand-shadow;
```


**Ferramentas [Tools]:**

Nessa camada usamos a grande utilidade de um pré-processado de CSS, que são as funções e mixins.

```
@mixin vertical-align {
  position: relative;
  top: 50%;
  @include transform(translateY(-50%));
}
```


**Genéricos [Generic]:**

Box-sizing, reset, modernizr se encontram nessa camada. 

```
html {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
}

* {

    &,
    &:before,
    &:after {
        -webkit-box-sizing: inherit;
           -moz-box-sizing: inherit;
                box-sizing: inherit;
        }

}
```


**Base [Base]:**

Estilos que são aplicados nas tags HTML (estilização básica).

```
fieldset {
  background-color: lighten($base-border-color, 10%);
  border: $base-border;
  margin: 0 0 $small-spacing;
  padding: $base-spacing;
}

input,
label,
select {
  display: block;
  font-family: $base-font-family;
  font-size: $base-font-size;
}
```


**Objetos [Objects]:**

Seguindo o conceito de orientação a objeto CSS, nessa camada usamos as classes para os objetos que se repetem no projeto. A partir dessa camada que começamos a utilizar as classes CSS.


```
.o-media {
    display: table;
    width: 100%;
}

.o-layout {
    margin:  0;
    padding: 0;
    list-style: none;
    margin-left: -$base-spacing-unit;
}
```


**Componentes [Components]:**

Elementos de interface com contexto específico 

```
.c-avatar {
    display: block;
    border-radius: 100%;
    width:  64px;
    height: 64px;
}

.c-avatar--small {
    width:  32px;
    height: 32px;
}

.c-btn {
    display: inline-block;
    padding: (0.5 * $base-spacing-unit) $base-spacing-unit;
    text-decoration: none;
    background-color: $color-btn;
    color: #fff;
    border-radius: $base-radius;
    transition: background-color 0.2s;

    &:hover,
    &:focus {
        background-color: $color-btn-hover;
        color: #fff;
    }

}
```


**Tema (opcional) [Theme]:**

Como está bem sugestivo, seria a camada onde teremos os temas do projeto / componentes / objetos.


```
/**
 * Background colors.
 */
@each $color in $colors {

    $color-name:  nth($color, 1);
    $color-value: nth($color, 2);

    .bg--#{$color-name} {
        background-color: #{$color-value};
    }

}
```


**Trunfos [Trumps]:**

Especificidade mais alta, ajudantes e substituições (.col-ms-12 {})

```
.clearfix,
%clearfix {

    &:after {
        content: "";
        display: table;
        clear: both;
    }

}
```


### Guidelines
O [CSS Guidelines](http://cssguidelin.es/) é uma documentação de recomendações e abordagens feita por Harry Roberts, para ajudar no desenvolvimento de um código CSS, legível, escalável, de fácil manutenção e etc..


### Framework
O [Inuitcss](https://github.com/inuitcss) é desenvolvido utilizando o pré-processador [Sass](http://sass-lang.com/), com a convenção de nomenclatura [BEM](https://en.bem.info/method/) e a metodologia [OOCSS](https://github.com/stubbornella/oocss/wiki) que não impõe o design. Projetado com o pensamento em escalabilidade e desempenho.

**Obs:** A única camada com a qual o Inuitcss não se envolve é a de componentes.


## Conclusão

Tenho adotado essa metodologia nos meus projetos, e tenho achado bem satisfatório a organização e o padrão que o mesmo traz.

O InuitCSS, utiliza da metodologia do [BEM](https://en.bem.info/method/). Acho muito confuso, misturar undercore com hífen. Por isso, utilizo assim:

```
.form {} // bloco
.form-field {} // elemento
.form-field--select {} // modificador
```

Para blocos com nome composto, ainda estou refletindo sobre o assunto, mas no momento utilizo tudo em minúsculo. Aceito feedbacks sobre o assunto.

```
.singleform {}
```

Quanto a estrutura de pastas, tenho usado numeração nas mesmas para uma melhor visualização.

- 00-settings
- 01-tools
- 03-base
- etc..

E com o acréscimo da pasta:

- vendor - para CSS externos de plugins (ex: colorbox, bxslider)

Acredito que não tem uma metodologia com a verdade absoluta e que o melhor caminho é ver o que o projeto necessita. Como pode ser visto neste artigo, a ideia era falar de uma metodologia, mas você pode ver que outras foram citadas e ideias acrescentadas. O mais importante é a unificação de pensamento da equipe para o projeto evoluir com um padrão sólido.


## Referências

- [Modular-CSS](https://github.com/bernarddeluna/Modular-CSS)
- [CSS Modular – Breve explicação](http://tableless.com.br/css-modular-breve-explicacao/)
- [CSS escalável - Parte 1](https://medium.com/@shankarcabus/css-escalavel-parte-1-41e7e863799e)
- [Classes Funcionais](http://tableless.com.br/classes-funcionais/)
- [Arquitetura CSS](http://tableless.com.br/arquitetura-css-anotacoes-da-palestra-rafael-rinaldi/)
- [InuitCSS](https://github.com/inuitcss)
- [css-scaffold](https://github.com/csshugs/css-scaffold)
- [Inuitcss for noobs](http://desgnl.github.io/inuitcss-demo/)
- [itcss-netmag](https://github.com/itcss/itcss-netmag)
- [the-apparatus](https://github.com/Steviant/the-apparatus)
- [TemplateITCss](https://github.com/lionelthonon/TemplateITCss)
- [BEM](https://en.bem.info/method/)