title: Ferramentas para o workflow front-end
date: 2015-02-14 14:35:48
category: front-end
tags:
excerpt: Antes de qualquer coisa, cada um/equipe sabe o que é melhor para o workflow do seu projeto. Cada projeto tem suas necessidades e o ideal que seja feito um planejamento para o mesmo.
---

Antes de qualquer coisa, cada um/equipe sabe o que é melhor para o workflow do seu projeto. Cada projeto tem suas necessidades e o ideal que seja feito um planejamento para o mesmo. O que interessa é a organização, automação das tarefas, gerenciamento de dependências e padrão único da equipe.

Para iniciar o fluxo de trabalho, temos que criar um controle de versão. Afinal, não podemos perder o que fizermos no projeto.

A partir desse momento vem o processo de desenvolvimento de código, com uma arquitetura sólida e preferencialmente com [TDD](http://pt.wikipedia.org/wiki/Test_Driven_Development) ou [BDD](http://pt.wikipedia.org/wiki/Behavior_Driven_Development).

O importante é a criação de padrões e documentação. Para que o projeto fique de fácil compreensão na hora da manutenção. Os testes unitários, automatização de tarefas e ferramentas que auxiliam na depuração são de grande valia para assegurar que o seu código seja estável.

Como não queria que o artigo ficasse muito extenso, nos próximos artigos vou falar mais detalhadamente sobre cada etapa e ferramenta. Por enquanto, listei por categorias o que uso no fluxo de trabalho como front-ender, com pequenos comentários.

***

## Linguagem

As linguagens que nós front-enders mais usamos, são:

*   **HTML** - [http://pt.wikipedia.org/wiki/HTML](http://pt.wikipedia.org/wiki/HTML)
*   **CSS** - [http://pt.wikipedia.org/wiki/Cascading_Style_Sheets](http://pt.wikipedia.org/wiki/Cascading_Style_Sheets)
*   **JavaScript** - [http://pt.wikipedia.org/wiki/JavaScript](http://pt.wikipedia.org/wiki/JavaScript)

***

## Diversos e Produtividade

Como editor utilizei por um bom tempo o Sublime e o Bracket, no momento utilizando o Atom. Ter um bom editor que auxilia com seus plugins, facilita e agiliza muito o processo de desenvolvimento.

*   **Sublime Text** - [http://www.sublimetext.com/3](http://www.sublimetext.com/3)
*   **Bracket** - [http://brackets.io/](http://brackets.io/)
*   **Atom** - [https://atom.io/](https://atom.io/)


Para gerenciar as tarefas:

*   **Wunderlist** - [https://www.wunderlist.com/pt/](https://www.wunderlist.com/pt/)


Poderia colocar tudo no mesmo lugar, mas tenho a preferência de versionar os arquivos gráficos como os layouts em PSD no Dropbox,  documentos e livros no Google Drive.

*   **Dropbox** - [https://www.dropbox.com/](https://www.dropbox.com/)

*   **Google Drive** - [https://www.google.com/intl/pt-BR/drive/](https://www.google.com/intl/pt-BR/drive/)

***

## Softwares

Gosto de usar o Rails Installer, pois em uma só tacada ele já instala o Ruby, Rails, Git bash e etc..

*   **Rails Installer** - [http://railsinstaller.org/pt-BR](http://railsinstaller.org/pt-BR)


Para criar, visualizar e editar os layouts web.

*   **Photoshop** - [http://www.adobe.com/br/products/photoshop.html](http://www.adobe.com/br/products/photoshop.html)

***

## Referências

Ótimas referências para consultar a compatibilidade crossbrowser e novos recursos.

*   **MDN** - [https://developer.mozilla.org/pt-BR/](https://developer.mozilla.org/pt-BR/)

*   **caniuse** - [http://caniuse.com/](http://caniuse.com/)

*   **html5please** - [http://html5please.com/](http://html5please.com/)

*   **Cross Browser Knowledgebase** - [http://www.crossbrowserbook.com/Knowledge](http://www.crossbrowserbook.com/Knowledge)

***

## Ferramentas Online

Para fazer testes rápidos com bibliotecas JavaScript, HTML e CSS

*   **jsfiddle** - [http://jsfiddle.net/](http://jsfiddle.net/)

Nem sempre conseguimos ter todos os ícones que precisamos em um lugar só, o Icomoon ajuda nisso e com ícones que você mesmo criou.

*   **icomoon** - [https://icomoon.io/](https://icomoon.io/)

Para testar a performance do site:

*   **PageSpeed** - [https://developers.google.com/speed/pagespeed/insights/](https://developers.google.com/speed/pagespeed/insights/)

*   **GTmetrix** - [http://gtmetrix.com/](http://gtmetrix.com/)

Avaliar a performance do seu código JavaScript.

*   **JSperf** - [http://jsperf.com/](http://jsperf.com/)

Depois de estruturar seu código HTML com o Schema, aqui você pode ver como o Google vê.

*   **Google Structured Data Testing Tool** - [http://www.google.com.br/webmasters/tools/richsnippets](http://www.google.com.br/webmasters/tools/richsnippets)

Para validar a acessibilidade do seu projeto.

*   **daSilva** - [http://www.dasilva.org.br/](http://www.dasilva.org.br/)

***

## Plataforma

Além de ser um pré-requisito para utilizar muitas ferramentas que estão listadas nesse artigo, o Node consegue levar o front-ender para o lado do servidor. Não vou falar muito porque isso merece um artigo à parte. ^^

*   **NodeJS** - [http://nodejs.org](http://nodejs.org)

***

## Geradores & Gerenciadores

Para conectar a criação de estrutura, automatização e gerenciamento de tarefas, temos:

*   **Yeoman** - [http://yeoman.io/](http://yeoman.io/)

Gerenciador de Pacotes do Node

*   **NPM** - [https://www.npmjs.com/](https://www.npmjs.com/)

Gerenciador de dependências

*   **Bower** - [http://bower.io/](http://bower.io/)

Para gerar páginas estáticas. Uso para este blog. :)

*   **Jekyll** - [http://jekyllrb.com/](http://jekyllrb.com/)

***

## Automatização de tarefas

Sempre utilizei o Grunt, mas já testando o Gulp. ^^

*   **Grunt** - [http://gruntjs.com/](http://gruntjs.com/http://gruntjs.com/)

*   **Gulp** - [http://gulpjs.com/](http://gulpjs.com/)

***

## Pré-processador de CSS

Sou grande adepto do SCSS, e utilizo bastante o Compass.

*   **SASS** - [http://sass-lang.com/](http://sass-lang.com/)

*   **Compass** - [http://compass-style.org/](http://compass-style.org/)

*   **LESS** - [http://lesscss.org/](http://lesscss.org/)

*   **Stylus** - [http://learnboost.github.io/stylus/](http://learnboost.github.io/stylus/)

***

## Versionamento

Para versionar os meus projetos utilizo o Git, dividindo entre o [Github](https://github.com/) e o [Bitbucket](https://bitbucket.org/)

*   **Git** - [http://git-scm.com/book/pt-br/v1](http://git-scm.com/book/pt-br/v1)


***

## Depuração & teste

Muito útil para garantir a qualidade e padrão do código.

*   **JSHint** - [http://jshint.com/](http://jshint.com/)

*   **CSSLint** - [http://csslint.net/](http://csslint.net/)


Teste unitário para o seu código JavaScript, é indispensável.

*   **Jasmine** - [http://jasmine.github.io/](http://jasmine.github.io/)

*   **Mocha** - [http://mochajs.org/](http://mochajs.org/)

*   **Karma** - [http://karma-runner.github.io/0.12/index.html](http://karma-runner.github.io/0.12/index.html)


Para visualização rápida em várias resoluções

*   **Responsive Design Testing** - [http://mattkersley.com/responsive/](http://mattkersley.com/responsive/)

***

## Navegador

Por questão de memória RAM, largando o Chrome como meu navegador principal para voltar para o Firefox.

*   **Chrome** - [http://www.google.com.br/chrome/browser/desktop/](http://www.google.com.br/chrome/browser/desktop/)

*   **Firefox** - [https://www.mozilla.org/pt-BR/firefox/new/](https://www.mozilla.org/pt-BR/firefox/new/)

***

## Complementos do navegador

*   **ColorZilla** - [http://www.colorzilla.com/](http://www.colorzilla.com/)

*   **Firebug** - [http://getfirebug.com/](http://getfirebug.com/)

*   **Window Resizer** - [Chrome](https://chrome.google.com/webstore/detail/window-resizer/kkelicaakdanhinjdeammmilcgefonfh) & [Firefox](https://addons.mozilla.org/pt-br/firefox/addon/window-resizer/)

*   **Chrome Developer Tools** - [https://developer.chrome.com/devtools](https://developer.chrome.com/devtools)

***

## Bibliotecas & Frameworks

O mais famoso e mais utilizado com certeza é o JQuery. Também utilizo no momento, mas já tive um bom contato com o Dojo e foi um preríodo de um bom conhecimento.

*   **jQuery** - [http://jquery.com/](http://jquery.com/)

*   **Dojo Toolkit** - [http://dojotoolkit.org/](http://dojotoolkit.org/)


Avaliando os prós e contras desses 2 frameworks que tem muito a acrescentar.

*   **AngularJS** - [https://angularjs.org/](https://angularjs.org/)

*   **React** - [http://facebook.github.io/react/](http://facebook.github.io/react/)

***

## Sintaxe & metodologias

Com uma junção de padrões e metodologias, você cria um boa estrutura para o seu projeto.

*   **BEM** - [https://bem.info/method/](https://bem.info/method/)

*   **OOCSS** - [http://oocss.org/](http://oocss.org/)

*   **SMACSS** - [https://smacss.com/](https://smacss.com/)

*   **DryCSS** - [http://malko.github.io/dryCss/](http://malko.github.io/dryCss/)

*   **Atomic Design** - [http://bradfrost.com/blog/post/atomic-web-design/](http://bradfrost.com/blog/post/atomic-web-design/)

*   **CSS Guidelines** - [http://cssguidelin.es/](http://cssguidelin.es/)

*   **Idiomatic CSS** - [https://github.com/necolas/idiomatic-css](https://github.com/necolas/idiomatic-css)

*   **Idiomatic SASS** - [https://github.com/anthonyshort/idiomatic-sass](https://github.com/anthonyshort/idiomatic-sass)

*   **Idiomatic JS** - [https://github.com/rwaldron/idiomatic.js](https://github.com/rwaldron/idiomatic.js)

*   **JavaScript design patterns** - [https://github.com/addyosmani/essential-js-design-patterns](https://github.com/addyosmani/essential-js-design-patterns)

***

## Documentação

Para a documentação do código SCSS e JS. Para as demais documentações, utilizo o Jekyll.

*   **SassDoc** - [http://sassdoc.com/](http://sassdoc.com/)

*   **JSDuck** - [https://github.com/senchalabs/jsduck](https://github.com/senchalabs/jsduck)

***

## Inspiração & Blogs

É sempre bom ter uma inspiração e segue algumas.

*   **CSS Tricks** - [http://css-tricks.com/](http://css-tricks.com/)

*   **Smashing Magazine** - [http://www.smashingmagazine.com/](http://www.smashingmagazine.com/)

*   **A List Apart** - [http://alistapart.com/](http://alistapart.com/)

*   **Codrops** - [http://tympanus.net/codrops/](http://tympanus.net/codrops/)

*   **Awwwards** - [http://www.awwwards.com/](http://www.awwwards.com/)


Por enquanto é isso. :)