---
title: Serie conhecendo a linguagem Javascript - Valores
date: 2015-05-15 15:15:19
update: 2015-10-07 15:15:19
category: javascript
excerpt: ""
---

Agora que já conhecemos os tipos de dados que existem na linguagem JavaScript, agora precisamos ver mais sobre os valores em si. Como no caso do tipo `boolean`, que tem apenas 2 valores possíveis, `true` ou `false`.

Podemos inverter o valor do tipo `boolean` com o operador `!`(negação). Ex:

```
var test = true;
console.log(!test); // false
```

Negando 2 vezes `!!`, podemos ter o retorno real do valor. Ex:

```
var test = true;
console.log(!!test); // true
```

Uma coisa muito importante para se levar em conta nas condições que você possa fazer no código JavaScript, é se determinados valores são considerados verdadeiros ou falsos. 

Considerados `falsy` (falsos):

- false
- 0 (zero)
- "" (string vazia)
- null
- undefined
- NaN

```
if(false) console.log('true');
else console.log('false');

if(0) console.log('true');
else console.log('false');

if("") console.log('true');
else console.log('false');

if(null) console.log('true');
else console.log('false');

if(undefined) console.log('true');
else console.log('false');

if(NaN) console.log('true');
else console.log('false');
```


Considerados `truthy` (verdadeiros):

- "0" (zero)
- "false"
- function() {}
- []
- {}

```
if("0") console.log('true');
else console.log('false');

if("false") console.log('true');
else console.log('false');

if(function(){}) console.log('true');
else console.log('false');

if([]) console.log('true');
else console.log('false');

if({}) console.log('true');
else console.log('false');
```

Também é importante conhecer os possíveis valores do tipo `number`.

- Inteiro
```
var num = 10;
var num2 = -10;
```

- Decimal
```
var num = 10.12;
var num2 = -10.12;
```

- Com expoente
```
var num = 421e6
console.log(num); //421000000

var num2 = 421e-6
console.log(num2); //0.000421
```

- Hexadecimal
```
var num = 0xFF;
console.log(num); // 255
```



<br><br>
### Série Conhecendo a linguagem JavaScript

*	1.[Introdução](/javascript/serie-conhecendo-a-linguagem-javascript-introducao)
*	2.[Tipos de dados](/javascript/serie-conhecendo-a-linguagem-javascript-tipos-de-dados)
*	3.Valores
*	4.[Variáveis](/javascript/serie-conhecendo-a-linguagem-javascript-variaveis)