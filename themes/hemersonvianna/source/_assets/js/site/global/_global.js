$(function(){
    'use strict';

    $('.js-mobilemenu').on('click', function( event ){
		event.preventDefault();
        $('body').toggleClass('mobilemenuactive');
    });

	if( $.cookie('contrast') === 'true' ){
		$('body').addClass('contrast');
	}

	$('.js-contrast').on('click', function( event ){
		event.preventDefault();
        $('body').toggleClass('contrast');

		if( $.cookie('contrast') === 'true' ){
			$.removeCookie('contrast', { path: '/' });
		} else {
			$.cookie('contrast', 'true', { path: '/' });
		}
    });

	$('.js-scroll').on('click', function( event ){
		event.preventDefault();

		var target = $(this).data('target');
        $('html, body').stop().animate({
			scrollTop: $( '.'+target ).offset().top
		}, 1500);
    });


	if ( annyang && $('.home').length ) {
		annyang.setLanguage('pt-br');

//		function retira_acentos_voz(palavra) {
//			com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ ,.;/';
//			sem_acento = 'aaaaaeeeeiiiiooooouuuucaaaaaeeeeiiiiooooouuuuc-----';
//
//			nova='';
//			for(i=0;i<palavra.length;i++) {
//			  if (com_acento.search(palavra.substr(i,1))>=0) {
//				nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1);
//			  }
//			  else {
//			   nova+=palavra.substr(i,1);
//			 }
//			}
//			return nova.toLowerCase();
//		}

		var commands = {
			'conteúdo': function() {
				$('.js-scroll').trigger('click');
			},
			'contraste': function() {
				$('.js-contrast').trigger('click');
			},
			'acessibilidade': function() {
				$('.js-accessibility').trigger('click');
			}
		};

		//annyang.debug();
		// Add our commands to annyang
		annyang.addCommands(commands);
		// Start listening.
		annyang.start();
	}

	// Twitter
	if( $('#tweets').length ){
		var config1 = {
			"id": '564168214889103361',
			"domId": 'tweets',
			"maxTweets": 3,
			"enableLinks": true,
			"showImages": false,
			"printUser": false
		};
		twitterFetcher.fetch(config1);
	}

	// Links externos dos posts
	$('.post-content a').each(function(){
		var
			expression = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
			, value    = $(this).attr('href')
		;

		if( expression.test( value ) ){
			$(this).attr('target', '_blank');
		}
	});
});
