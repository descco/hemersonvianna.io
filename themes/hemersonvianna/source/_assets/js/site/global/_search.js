(function (){
    'use strict';

    var url = window.location.protocol + "//" + window.location.host;

    // the root of your site
    var root = '/site/';
    // the path of the search data
    var path = url + root + 'search.xml';
    // the search input id
    var search_id = 'my-search-input';
    // the search result id
    var content_id = 'search-content';

    $.ajax({
        url: path,
        dataType: "xml",
        success: function( xmlResponse ) {
            // get the contents from search data
            var datas = $( "entry", xmlResponse ).map(function() {
                return {
                    title: $( "title", this ).text(),
                    content: $("content",this).text(),
                    url: $( "url" , this).text()
                };
            }).get();

            var $input = document.getElementById(search_id);
            var $searchContent = document.getElementById(content_id);

            $input.addEventListener('input', function(){
                var str='';
                var flags = [];          // for tagging articles that fullfill the query
                var content_index = [];
                var keywords = this.value.trim().split(/[\s\-]+/);

                $searchContent.innerHTML = "";

                // perform local searching
                keywords.forEach(function(keyword){
                    if(keyword !== ''){
                        var keyword_len = keyword.length;
                        datas.forEach(function(data , i){
                            var data_title = data.title.trim();
                            var data_content = data.content.trim().replace(/<[^>]+>/g,"");
                            var index_title = -1;
                            var index_content = -1;

                            // only match artiles with not empty titles and contents
                            if(data_title !== '' && data_content !== ''){
                                index_title = data_title.indexOf(keyword);
                                index_content = data_content.indexOf(keyword);
                            }

                            if( index_title >= 0 || index_content >= 0 ){
                                if(flags.indexOf(i) === -1) {
                                    flags.push(i);
                                    content_index.push({index_content:index_content, keyword_len:keyword_len});
                                }
                            }
                        });
                    }
                });

                // show search results
                if(flags !== []){
                    flags.forEach(function(flag , i){
                        var post_url = datas[flag].url;
                        var post_title = datas[flag].title;

                        str += "<li><a href='"+ post_url +"' class='list-title'>"+ post_title +"</a>";

                        var content = datas[flag].content.trim().replace(/<[^>]+>/g,"");
                        var ind = content_index[i].index_content;
                        var keyword_len = content_index[i].keyword_len;

                        if(ind >= 0){
                            // cut out 100 characters
                            var start = ind - 20;
                            var end = ind + 80;

                            if(start < 0){
                                start = 0;
                            }

                            if(start === 0){
                                end = 100;
                            }

                            if(end > content.length){
                                end = content.length;
                            }

                            var before_content = content.substr(start, ind - start);
                            var key_content = content.substr(ind, keyword_len);
                            var after_content = content.substr(ind + keyword_len, end - ind - keyword_len);

                            str += "<p" +" class='list-content'>" + before_content;
                            str += "<span" + " class='span-content'" + ">" + key_content +"</span>";
                            str += after_content +"...</p>";
                        }

                        str += "</li>";
                    });
                }

                $searchContent.innerHTML = str;
            });
        }
    });

})();