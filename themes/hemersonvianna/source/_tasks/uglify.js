module.exports = function(grunt, options){

  var projectDev = options.projectDev;
  var projectDir = options.projectDir;
  var projectVendor = options.projectVendor;

  return {
    options: {
      mangle: {
        except: ['jQuery']
      }
    },
    site: {
      files: {
        '<%= projectDir %>js/site/scripts.min.js': 
        [
        '<%= projectVendor %>jquery/dist/jquery.min.js',
        '<%= projectDev %>js/site/vendor/_annyang.js',
        '<%= projectDev %>js/site/vendor/_cookie.js',
        '<%= projectDev %>js/site/app/_module.js',
        '<%= projectDev %>js/site/app/_analytics.js',
        '<%= projectDev %>js/site/app/_detection.js',
        '<%= projectDev %>js/site/app/_mediator.js',
        '<%= projectDev %>js/site/app/_plugins.js',
        '<%= projectDev %>js/site/global/_geolocalization.js',
        '<%= projectDev %>js/site/global/_global.js',
        '<%= projectDev %>js/site/social/_twitter.js',
        '<%= projectDev %>js/site/_app.js',
        ]
      }
    }
  };
};